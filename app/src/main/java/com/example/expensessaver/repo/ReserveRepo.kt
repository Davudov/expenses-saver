package com.example.expensessaver.repo

import androidx.lifecycle.MutableLiveData
import com.example.expensessaver.api.Result
import com.example.expensessaver.entity.Category
import com.example.expensessaver.entity.CategoryLimits
import com.example.expensessaver.entity.ReserveCapital
import com.example.expensessaver.entity.ReserveCapitalDetails
import com.example.expensessaver.storage.CapitalDao
import com.example.expensessaver.ui.categories.category_details.CapitalExpense
import timber.log.Timber

class ReserveRepo(private val capitalDao: CapitalDao) {

    val reservedCapitalList = capitalDao.getAll()
    val resTotalExpense = MutableLiveData<ReserveCapitalDetails>()

    val categoryList = capitalDao.getAllCategory()
    val availableCategoryList = capitalDao.getAvailableCategories()

    val expenseByCapitalDate = MutableLiveData<List<CapitalExpense>>()

    suspend fun insertCapital(
        capital: ReserveCapital,
        currentList: MutableList<CategoryLimits>
    ): Result<Boolean> {
        return try {
            val rowId = capitalDao.insert(capital)
            Timber.d("Database ~ capital inserted $rowId")
            currentList.forEach { it.capitalId = rowId.toInt() }
            insertCatLimit(currentList)
            Result.Success(true)
        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }


    suspend fun getDataByCategory(category: Int): Result<Boolean> {
        return try {
            val categoryLimits = capitalDao.getCapitalByCategoryDetails(category)
            expenseByCapitalDate.postValue(categoryLimits.map { CapitalExpense.Total(it) })
            Result.Success(true)
        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }


    suspend fun delete(capital: ReserveCapital, adapterPosition: Int): Result<Int> {
        return try {
            capitalDao.deleteCapital(capital)
            Result.Success(adapterPosition)
        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }

    suspend fun deleteCategory(category: Category, adapterPosition: Int): Result<Int> {
        return try {
            capitalDao.deleteCategory(category)
            Result.Success(adapterPosition)
        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }


    suspend fun insertCategory(category: String): Result<Boolean> {
        return try {
            val row = capitalDao.insertCategory(Category(0, category))
            Timber.d("Database ~ category inserted $row")
            Result.Success(true)
        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }
    suspend fun getTotalExpenseByCapital(capitalID: Int) {
         try {
           val totalSUm = capitalDao.getTotalExpenseByCapital(capitalID)
             resTotalExpense.postValue(totalSUm)
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    suspend fun insertCatLimit(currentList: MutableList<CategoryLimits>) {
        try {
            capitalDao.insertCatLimit(currentList)
            Timber.d(" c completed ")
        } catch (e: java.lang.Exception) {
            Timber.e(e)
        }

    }

}

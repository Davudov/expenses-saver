package com.example.expensessaver.repo

import androidx.lifecycle.MutableLiveData
import com.example.expensessaver.api.Result
import com.example.expensessaver.entity.Expense
import com.example.expensessaver.storage.ExpenseDao
import timber.log.Timber

class ExpenseRepo(private val dao: ExpenseDao) {

    val listLiveData = dao.getAll()

     val listByCatCapLiveData = MutableLiveData<List<Expense>>()

    suspend fun getListByCatCapLiveData(categoryId:Int,capitalId:Int):Result<Boolean> {
       return try {
            val res = dao.getAllByCatCap(categoryId, capitalId)
            listByCatCapLiveData.postValue(res)
            Result.Success(true)
        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }


        suspend fun insertItem(expense: Expense): Result<Boolean> {
            return try {
                dao.insert(expense)
                Result.Success(true)
            } catch (e: Exception) {
                Timber.e(e)
                Result.Error(e)
            }
        }

        suspend fun delete(expense: Expense, adapterPosition: Int): Result<Int> {
            return try {
                dao.deleteExpense(expense)
                Result.Success(adapterPosition)
            } catch (e: Exception) {
                Timber.e(e)
                Result.Error(e)
            }
        }


    }
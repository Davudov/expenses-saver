package com.example.expensessaver.extensions

import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.example.expensessaver.R
import com.example.expensessaver.util.ServiceUtil
import timber.log.Timber

@BindingAdapter("app:double_text")
fun TextView.setDoubleText(value: Double) {
    try {
        val strDouble = value.toString()
        text = context.getString(R.string.amount_text_view, strDouble)
    } catch (e: Exception) {
        Timber.e(e)
        text = context.getString(R.string.double_parse_error)
    }
}
@BindingAdapter("app:differ_amounts")
fun TextView.setDifferenceAmount(result:Double) {
    try {
        val strDouble = result.toString()
        text = context.getString(R.string.amount_text_view, strDouble)
        if (result < 0){
            setBackgroundColor(ContextCompat.getColor(context,R.color.limit_exceed_color))
        }else{
            setBackgroundColor(ContextCompat.getColor(context,R.color.limit_not_exceed_color))
        }
    } catch (e: Exception) {
        Timber.e(e)
        text = context.getString(R.string.double_parse_error)
    }
}
@BindingAdapter("app:float_text")
fun TextView.setDoubleText(value: Float) {
    try {
        val strFloat = value.toString()
        text = context.getString(R.string.amount_text_view, strFloat)
    } catch (e: Exception) {
        Timber.e(e)
        text = context.getString(R.string.double_parse_error)
    }
}

fun View.hideKeyboard() {
    val imm: InputMethodManager = ServiceUtil.getInputMethodManager(context)
    imm.hideSoftInputFromWindow(windowToken, 0)
}

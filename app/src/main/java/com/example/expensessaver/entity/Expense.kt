package com.example.expensessaver.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.example.expensessaver.ui.details.sdf
import kotlinx.android.parcel.Parcelize
import timber.log.Timber
import java.util.*

@Entity
@Parcelize
class Expense(
    @PrimaryKey(autoGenerate = true)
    val expense_id: Int = 0,
    @ColumnInfo(name = "expense_amount")
    var amountDouble: Double,
    @ColumnInfo(name = "expense_details")
    var details: String,
    @ColumnInfo(name = "category_name")
    var categoryName: String,
    @ColumnInfo(name = "expense_category_id")
    var categoryId: Int,
    @ColumnInfo(name = "expense_created_date_millis")
    var expenseDateMillis:Long,
    @ColumnInfo(name = "expense_related_capital_id")
    var expense_related_capital_id:Int
) : Parcelable {

    @Ignore
    var amountStr: String = ""
    @Ignore
    var date: String = sdf.format(Date(expenseDateMillis))
}

fun String.convertStrToDoubleSafely(): Double {
    return try {
        toDouble()
    } catch (e: Exception) {
        Timber.e(e)
        .0
    }
}

fun String.convertStrToFloatSafely(): Float {
    return try {
        toFloat()
    } catch (e: Exception) {
        Timber.e(e)
        .0f
    }
}



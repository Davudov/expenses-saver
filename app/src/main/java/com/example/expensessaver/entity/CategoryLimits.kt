package com.example.expensessaver.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Entity
@Parcelize
class CategoryLimits(
    @PrimaryKey(autoGenerate = true)
    val category_limit_id: Int = 0,
    @ColumnInfo(name = "category_limit_category_id")
    val categoryId: Int,
    @ColumnInfo(name = "category_limit_reserve_capital_id")
    var capitalId: Int,
    @ColumnInfo(name = "category_limit")
    var categoryLimit: Double,
    @ColumnInfo(name = "category_limit_category_name")
    val categoryName: String,
    @ColumnInfo(name = "category_limit_total_expenditure")
    val totalExpenditure: Double,
):Parcelable{
    @Ignore
    val resultExpenditureDif: Double = categoryLimit.toDouble() - totalExpenditure

}

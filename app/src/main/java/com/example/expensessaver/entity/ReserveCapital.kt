package com.example.expensessaver.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.example.expensessaver.ui.details.sdf
import kotlinx.android.parcel.Parcelize
import java.util.*


@Entity
@Parcelize
class ReserveCapital(
    @PrimaryKey(autoGenerate = true)
    val reserve_capital_id: Int = 0,
    @ColumnInfo(name = "capital_amount")
    var amount: Double,
    @ColumnInfo(name = "capital_details")
    var details: String,
    @ColumnInfo(name = "capital_category")
    var category: String,
    @ColumnInfo(name = "capital_created_date_millis")
    var reserveCapitalDateMillis:Long
) : Parcelable {

    @Ignore
    var amountStr: String = ""
    @Ignore
    var date: String = sdf.format(Date(reserveCapitalDateMillis))
}
data class ReserveCapitalDetails(val total_expense_amount:Double, val capital_amount:Double)
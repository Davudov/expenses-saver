package com.example.expensessaver.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity( foreignKeys = [ForeignKey(entity = Expense::class,
    parentColumns = arrayOf("expense_id"),
    childColumns = arrayOf("expense_child_id"),
    onDelete = ForeignKey.CASCADE),
    ForeignKey(entity = ReserveCapital::class,
    parentColumns = arrayOf("reserve_capital_id"),
    childColumns = arrayOf("reserve_capital_child_id"),
    onDelete = ForeignKey.CASCADE)]
)
@Parcelize
class ExpensesReservedCapitalEntity(
    @PrimaryKey(autoGenerate = true)
    val ExpensesReservedCapitalId: Long,
    @ColumnInfo(name = "category")
    var category: String,
    @ColumnInfo(name = "expense_child_id", index = true)
    var expenseId :String,

    @ColumnInfo(name = "reserve_capital_child_id" , index = true)
    var capitalId:String

):Parcelable{

}
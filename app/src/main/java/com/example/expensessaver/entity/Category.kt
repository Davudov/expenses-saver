package com.example.expensessaver.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Category(
    @PrimaryKey(autoGenerate = true)
    val category_id: Int = 0,
    @ColumnInfo(name = "category_name")
    val categoryName: String
):Parcelable
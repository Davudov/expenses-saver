package com.example.expensessaver.entity

import androidx.room.Ignore
import com.example.expensessaver.ui.details.sdf
import java.util.*

data class ExpensesAndCapital(
    val expense_amount: Double,
    val capital_amount: Float,
    val capital_created_date_millis: Long,
    val reserve_capital_id: Int,

    )

data class CategoryLimitCapital(
    val category_limit: Int,
    val reserve_capital_id: Int
)

data class CapitalDetails(
    val category_limit: Double,
    val category_limit_total_expenditure: Double,
    val capital_amount: Double,
    val capital_created_date_millis: Long,
    val category_limit_category_id: Int,
    val reserve_capital_id: Int

    ) {
    @Ignore
    var date: String = sdf.format(Date(capital_created_date_millis))
}




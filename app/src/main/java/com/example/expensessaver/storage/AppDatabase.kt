package com.example.expensessaver.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.expensessaver.entity.*


@Database(
    exportSchema = true,
    version = 2,
    entities = [Expense::class, ReserveCapital::class, ExpensesReservedCapitalEntity::class, Category::class, CategoryLimits::class]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun expenseDao(): ExpenseDao
    abstract fun capitalDao(): CapitalDao

    companion object {
        var name = "database"
    }
}

package com.example.expensessaver.storage

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.expensessaver.entity.Expense
import timber.log.Timber

@Dao
interface ExpenseDao {

    @Query("SELECT * FROM expense order by expense_created_date_millis DESC")
    fun getAll(): LiveData<List<Expense>>

    @Query("SELECT * FROM expense where expense_related_capital_id =:reserveCapitalId and expense_category_id = :categoryId order by expense_created_date_millis DESC ")
    suspend fun getAllByCatCap(categoryId: Int,reserveCapitalId:Int): List<Expense>

//    @Query("SELECT * FROM expense order by expense_created_date_millis DESC")
//    fun getAll1(): LiveData<List<Expense>>

//    @Query("SELECT sum(expense_amount) FROM expense where created_date_millis >= ''")
//    fun getSumOfExpenses(): LiveData<Int>


    @Query("SELECT reserve_capital_id FROM reservecapital where reservecapital.capital_created_date_millis <= :time order by capital_created_date_millis desc")
    suspend fun getLatestCapitalId(time: Long): Int

//    @Query("INSERT INTO expensesreservedcapitalentity  (category,expense_child_id,reserve_capital_child_id) VALUES (:category,:expenseId,:capitalId)")
//    suspend fun insertExpenseByCategory(expenseId: Long, capitalId: Long, category: String): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertExpense(expense: Expense): Long

    @Query("update categorylimits set category_limit_total_expenditure = category_limit_total_expenditure + :exp where category_limit_category_id = :categoryId and category_limit_reserve_capital_id = :capitalId")
    fun addExpToTotal(exp: Double, categoryId: Int, capitalId: Int)

    @Delete
    suspend fun deleteItem(item: Expense)

    @Transaction
    suspend fun deleteExpense(item: Expense){
        val deletedItemExpenseAmountDouble = item.amountDouble
        val categoryTotalExpense = getCategoryTotalExpense(item.categoryId)
        val resultExpenseAmount = categoryTotalExpense - deletedItemExpenseAmountDouble
        setCategoryTotalExpense(resultExpenseAmount)
        deleteItem(item)
    }

    @Query("update categorylimits set category_limit_total_expenditure = :resultExpenseAmount")
    fun setCategoryTotalExpense(resultExpenseAmount: Double)

    @Query("select category_limit_total_expenditure from categorylimits where category_limit_category_id = :categoryId")
    fun getCategoryTotalExpense(categoryId: Int): Double

    @Transaction
    suspend fun insert(expense: Expense) {
        val capitalId = getLatestCapitalId(expense.expenseDateMillis)
        expense.expense_related_capital_id = capitalId
        val id = insertExpense(expense)
        Timber.d("insert expense id  -> $id")
        Timber.d("insert expense cat id  -> ${expense.categoryId}")
        addExpToTotal(expense.amountDouble,expense.categoryId,capitalId)
    }


//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    suspend fun saveList(list: List<Asteroid>)
//
//    @Query("SELECT * FROM asteroid where closeApproachDate between :startDate and :endDate order by closeApproachDate")
//    suspend fun getDataByDate(startDate: String, endDate: String): List<Asteroid>

}
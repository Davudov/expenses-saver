package com.example.expensessaver.storage

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.expensessaver.entity.*

@Dao
interface CapitalDao {

    @Query("SELECT * FROM reservecapital order by capital_created_date_millis DESC")
    fun getAll(): LiveData<List<ReserveCapital>>

    @Query("SELECT SUM(expense_amount) as total_expense_amount, capital_amount FROM expense, reservecapital where expense_related_capital_id = :id")
    suspend fun getTotalExpenseByCapital(id:Int): ReserveCapitalDetails

    @Query("SELECT * FROM reservecapital order by  reserve_capital_id desc")
    suspend fun getAllReserveSuspend(): List<ReserveCapital>

//    @Query("SELECT MAX(reserve_capital_id) FROM reservecapital")
//    suspend fun getAllAllocSize(): Int


    @Query("SELECT * FROM categorylimits")
    fun getAllCategoryLimitsLive(): LiveData<List<CategoryLimits>>

//    @Query("SELECT category_limit,reserve_capital_id  FROM categorylimits join reservecapital where category_limit_category_id = :categoryId and reserve_capital_id = category_limit_reserve_capital_id")
//    suspend fun getAllCategoryLimits(categoryId: Int): List<CategoryLimitCapital>

    @Query("SELECT * FROM category")
    fun getAllCategory(): LiveData<List<Category>>

    @Query("SELECT category_name, category_id FROM category inner  join categorylimits where category_id = category_limit_category_id group by category_id")
    fun getAvailableCategories(): LiveData<List<Category>>

//    @Query("SELECT COUNT(reserve_capital_id) FROM reservecapital")
//    suspend fun getCapitalListSize(): Int

//    @Query("SELECT  category_limit,expense_category_id,reserve_capital_id,capital_amount,expense_amount,capital_created_date_millis FROM expense  join reservecapital join categorylimits where expense_category_id =:categoryId and expense_related_capital_id = reserve_capital_id and expense_related_capital_id = category_limit_reserve_capital_id and  category_limit_category_id =:categoryId order by reserve_capital_id desc")
//    suspend fun getCapExp(categoryId: Int): List<ExpensesAndCapital>

    @Query("select category_limit_category_id,reserve_capital_id,capital_created_date_millis,category_limit,category_limit_total_expenditure,capital_amount from reservecapital join categorylimits where category_limit_category_id =:categoryId and reserve_capital_id = category_limit_reserve_capital_id")
 suspend   fun getCapitalByCategoryDetails(categoryId:Int):List<CapitalDetails>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(capital: ReserveCapital): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategory(category: Category): Long


    @Delete
    suspend fun deleteItem(capital: ReserveCapital)

    @Delete
    suspend fun deleteCategoryItem(category: Category)

    @Query("delete from expense where expense_related_capital_id =:capitalId")
    fun deleteRelatedExpenses(capitalId: Int)

    @Query("delete from categorylimits where category_limit_category_id =:categoryId")
    fun deleteRelatedCategoryLimits(categoryId: Int)

    @Transaction
    suspend fun deleteCapital(capital: ReserveCapital) {
        deleteItem(capital)
        deleteRelatedExpenses(capital.reserve_capital_id)
        deleteRelatedCategoryLimits(capital.reserve_capital_id)
    }

    @Transaction
    suspend fun deleteCategory(category: Category) {
        deleteCategoryItem(category)
        deleteRelatedCategoryLimits(category.category_id)
    }


    @Insert
    suspend fun insertCatLimit(currentList: List<CategoryLimits>)

    @Query("select * from categorylimits")
    fun getCatLimit(): List<CategoryLimits>

}
package com.example.expensessaver.ui.details

import android.os.Parcelable
import androidx.core.os.LocaleListCompat
import com.example.expensessaver.entity.Expense
import com.example.expensessaver.entity.ReserveCapital
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat

val sdf = SimpleDateFormat("yyyy-MM-dd", LocaleListCompat.getDefault().get(0))
@Parcelize
class DetailFragmentArgumentEntity(
    val expense: Expense = Expense(0,0.0, "","", 0,0L,0),
    val isInsert: Boolean = false
) : Parcelable


@Parcelize
class ReserveDetailFragmentArgumentEntity(
    val capital: ReserveCapital = ReserveCapital( 0,.0, "","Gəlir",0L),
    val isInsert: Boolean = false
) :
    Parcelable
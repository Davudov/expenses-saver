package com.example.expensessaver.ui.categories.category_details

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CategoryDetailsFragmentArgumentEntity (val id:Int):Parcelable

package com.example.expensessaver.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.expensessaver.api.Result
import com.example.expensessaver.databinding.FragmentDetailsBinding
import com.example.expensessaver.util.ServiceUtil
import org.koin.androidx.viewmodel.ext.android.viewModel

class ExpenditureDetailsFragment : Fragment() {

    private val arg by navArgs<ExpenditureDetailsFragmentArgs>()

    private lateinit var binding: FragmentDetailsBinding

    private lateinit var navController: NavController

    private val viewModel: DetailsViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentDetailsBinding.inflate(inflater)

        navController = findNavController()

        binding.item = arg.item

        binding.viewModel = viewModel



        binding.editTextAmount.requestFocus()
        ServiceUtil.showKeyboard(binding.editTextAmount,requireContext())


        val arrayAdapter = SpinAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item
        )

        binding.spinnerCategory.adapter = arrayAdapter

        viewModel.availableCategoryList.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            arrayAdapter.addItems(it.toTypedArray())
        })


        binding.spinnerCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val category = arrayAdapter.getItem(position)
                category ?: return
                binding.item?.expense?.categoryName = category.categoryName
                binding.item?.expense?.categoryId = category.category_id
            }


            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }

        binding.spinnerCategory.setSelection(0)

        viewModel.insertionLiveData.observe(viewLifecycleOwner, Observer {
            when (it.action) {
                DetailsViewModel.ACTION.ON_SUCCESS -> when (it.result) {
                    is Result.Success -> {
                        navController.navigateUp()
                    }
                    is Result.Error -> {
                        Toast.makeText(
                            requireContext(),
                            it.result.exception.message,
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }
                DetailsViewModel.ACTION.ON_AMOUNT_EMPTY -> {
                    Toast.makeText(
                        requireContext(),
                        "Add amount to expenditure details",
                        Toast.LENGTH_LONG
                    ).show()
                    binding.editTextAmount.error = "Add amount"
                }
            }

        })
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }

    private fun hideKeyboard() {
        val imm: InputMethodManager = ServiceUtil.getInputMethodManager(
            requireContext()
        )
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)
    }


}

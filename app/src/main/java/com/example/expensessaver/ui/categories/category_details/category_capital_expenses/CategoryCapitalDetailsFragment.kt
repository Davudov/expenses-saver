package com.example.expensessaver.ui.categories.category_details.category_capital_expenses

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.example.expensessaver.R
import com.example.expensessaver.ui.expenditure.ExpenditureViewModel
import kotlinx.android.synthetic.main.layout_category_capital.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CategoryCapitalDetailsFragment : Fragment(R.layout.layout_category_capital) {
    private val arg by navArgs<CategoryCapitalDetailsFragmentArgs>()

    private val viewModel: ExpenditureViewModel by viewModel()


    val adapter = CategoryCapitalExpListAdapter()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recycler_view.adapter = adapter
        viewModel.getListByCatCapLiveData(arg.item.categoryId,arg.item.capitalId)
        viewModel.listByCatCapLiveData.observe(viewLifecycleOwner, Observer {
            it?:return@Observer
            adapter.insertData(it)
        })


    }

}
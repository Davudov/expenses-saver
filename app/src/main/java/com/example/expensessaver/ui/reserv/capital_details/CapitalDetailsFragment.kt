package com.example.expensessaver.ui.reserv.capital_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.expensessaver.databinding.LayoutCapitalDetailsBinding
import com.example.expensessaver.ui.reserv.ReserveViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class CapitalDetailsFragment : Fragment() {
    private lateinit var binding: LayoutCapitalDetailsBinding
    private val viewModel: ReserveViewModel by viewModel()
    private val arg by navArgs<CapitalDetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LayoutCapitalDetailsBinding.inflate(inflater)
        binding.viewModel = this.viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getTotalExpenseByCapital(arg.data.capitalId)
    }
}
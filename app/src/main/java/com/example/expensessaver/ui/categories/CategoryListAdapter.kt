package com.example.expensessaver.ui.categories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.expensessaver.databinding.ItemCategoryListBinding
import com.example.expensessaver.entity.Category


class CategoryListAdapter(
    val onClick: (category: Category) -> Unit,
    val onLongClick: (category: Category, pos: Int) -> Unit
) :
    ListAdapter<Category, RecyclerView.ViewHolder>(CategoryDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return from(parent)
    }

    fun from(parent: ViewGroup): CategoryViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding =
            ItemCategoryListBinding.inflate(layoutInflater, parent, false)
        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CategoryViewHolder -> {
                holder.bind(getItem(position))
            }
        }
    }

    inner class CategoryViewHolder(val binding: ItemCategoryListBinding) :
        RecyclerView.ViewHolder(binding.root) {



        fun bind(category: Category) {
            binding.item = category.categoryName
            binding.root.setOnClickListener { onClick(category) }
            binding.root.setOnLongClickListener() {
                onLongClick(category, adapterPosition)
                true
            }
            binding.executePendingBindings()
        }

    }

}


class CategoryDiffCallback : DiffUtil.ItemCallback<Category>() {
    override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
        return oldItem == newItem
    }
}

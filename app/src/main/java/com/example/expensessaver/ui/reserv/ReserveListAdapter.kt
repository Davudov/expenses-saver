package com.example.expensessaver.ui.reserv

import com.example.expensessaver.R
import com.example.expensessaver.base.BaseRecyclerViewAdapter
import com.example.expensessaver.entity.ReserveCapital

class ReserveListAdapter(onClick: (capital: ReserveCapital) -> Unit, onLongClick: (capital: ReserveCapital, pos:Int) -> Unit) :
    BaseRecyclerViewAdapter<ReserveCapital>(onClick,onLongClick) {
    override fun getLayoutRes(viewType: Int) = R.layout.list_item_reserve
}

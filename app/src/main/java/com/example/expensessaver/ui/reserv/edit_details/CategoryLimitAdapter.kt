package com.example.expensessaver.ui.reserv.edit_details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.expensessaver.databinding.ItemCategoryLimitListBinding
import com.example.expensessaver.entity.CategoryLimits
import timber.log.Timber

class CategoryLimitAdapter() :
    ListAdapter<CategoryLimits, RecyclerView.ViewHolder>(CategoryLimitDiffCallback()) {


    fun insertData(list: List<CategoryLimits>) {
        submitList(list)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return LimitViewHolder(
            ItemCategoryLimitListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LimitViewHolder -> {
                holder.bind(getItem(position))
            }
        }
    }


    inner class LimitViewHolder(val binding: ItemCategoryLimitListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CategoryLimits) {
            binding.data = item
            binding.addIv.setOnClickListener {
//                getItem(adapterPosition).categoryLimit =
//                    binding.categoryLimit.text.toString().toInt()

                try {
                    currentList.get(adapterPosition).categoryLimit =
                        binding.categoryLimit.text.toString().toDouble()
//                    notifyItemChanged(adapterPosition, binding.categoryLimit.text.toString().toInt())
                }
                catch (e:Exception){
                    Timber.e(e)
                    binding.categoryLimit.error = "Edit amount"
                }

                it.setBackgroundColor(
                    ContextCompat.getColor(
                        it.context,
                        android.R.color.holo_green_dark
                    )
                )
            }
//            binding.categoryLimit.setOnFocusChangeListener { v, hasFocus ->
//                Timber.d("hasFocus ${hasFocus}")
//                if (hasFocus){
//                    categoryLimitMap[item.category_id] = binding.categoryLimit.text.toString().toInt()
//                }
//            }
            binding.executePendingBindings()
        }
    }


    class CategoryLimitDiffCallback : DiffUtil.ItemCallback<CategoryLimits>() {
        override fun areItemsTheSame(oldItem: CategoryLimits, newItem: CategoryLimits): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: CategoryLimits, newItem: CategoryLimits): Boolean {
            return oldItem.category_limit_id == oldItem.category_limit_id
        }
    }
}
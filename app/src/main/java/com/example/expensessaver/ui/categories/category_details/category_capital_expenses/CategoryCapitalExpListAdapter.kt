package com.example.expensessaver.ui.categories.category_details.category_capital_expenses

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.expensessaver.databinding.ItemViewExpenseBriefBinding
import com.example.expensessaver.entity.Expense


//const val CAPITAL_VIEW_TYPE = 0
const val EXPENSE_VIEW_TYPE = 1
//const val TOTAL_EXP_VIEW_TYPE = 2

class CategoryCapitalExpListAdapter :
    ListAdapter<Expense, RecyclerView.ViewHolder>(CapitalDiffCallback()) {

    fun insertData(list: List<Expense>) {
        submitList(list)
    }

//    override fun getItemViewType(position: Int): Int {
//        return when (getItem(position)) {
////            is CapitalExpense.Capital -> CAPITAL_VIEW_TYPE
//            is Expense -> EXPENSE_VIEW_TYPE
////            is CapitalExpense.Total -> TOTAL_EXP_VIEW_TYPE
//        }
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        return when (viewType) {
//            CAPITAL_VIEW_TYPE -> {
//                CapitalViewHolder.from(parent)
//            }
//            EXPENSE_VIEW_TYPE -> {
                return ExpenseViewHolder.from(parent)
//            }
//            TOTAL_EXP_VIEW_TYPE -> {
//                TotalExpenseViewHolder.from(parent)
//            }
//            else -> throw ClassCastException("Unknown viewType ${viewType}")
//        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
//            is CapitalViewHolder -> {
//                holder.bind(getItem(position) as CapitalExpense.Capital)
//            }
            is ExpenseViewHolder -> {
                holder.bind(getItem(position))
            }
//            is TotalExpenseViewHolder -> {
//                holder.bind(getItem(position) as CapitalExpense.Total)
//            }
        }
    }

//    class CapitalViewHolder(val binding: ItemViewCapitalBinding) :
//        RecyclerView.ViewHolder(binding.root) {
//
//        companion object {
//            fun from(parent: ViewGroup): CapitalViewHolder {
//                val layoutInflater = LayoutInflater.from(parent.context)
//                val binding = ItemViewCapitalBinding.inflate(layoutInflater, parent, false)
//                return CapitalViewHolder(binding)
//            }
//        }
//
//        fun bind(capital: CapitalExpense.Capital) {
//            binding.capital = capital
//            binding.executePendingBindings()
//        }
//
//    }

    class ExpenseViewHolder(val binding: ItemViewExpenseBriefBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): ExpenseViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemViewExpenseBriefBinding.inflate(layoutInflater, parent, false)
                return ExpenseViewHolder(binding)
            }
        }

        fun bind(expense: Expense) {
            binding.expense = expense
            binding.number = adapterPosition
            binding.executePendingBindings()
        }

    }

//    class TotalExpenseViewHolder(val binding: ItemViewExpenseTotalBriefBinding) :
//        RecyclerView.ViewHolder(binding.root) {
//
//        companion object {
//            fun from(parent: ViewGroup): TotalExpenseViewHolder {
//                val layoutInflater = LayoutInflater.from(parent.context)
//                val binding =
//                    ItemViewExpenseTotalBriefBinding.inflate(layoutInflater, parent, false)
//                return TotalExpenseViewHolder(binding)
//            }
//        }
//
//        fun bind(total: CapitalExpense.Total) {
//            binding.item = total.capitalDetails
//            binding.executePendingBindings()
//        }
//
//    }
//
}


class CapitalDiffCallback : DiffUtil.ItemCallback<Expense>() {
    override fun areItemsTheSame(oldItem: Expense, newItem: Expense): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Expense, newItem: Expense): Boolean {
        return oldItem.expense_id == newItem.expense_id
    }
}

sealed class CapitalExpense {
//    class Capital(val capital: ReserveCapital) : CapitalExpense()
//    class Expense(val expenseNumber: Int, val expense: ExpensesAndCapital) : CapitalExpense()
//    class Total(val capitalDetails: CapitalDetails) : CapitalExpense()
}
package com.example.expensessaver.ui.expenditure

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.expensessaver.api.Result
import com.example.expensessaver.databinding.FragmentExpenditureBinding
import com.example.expensessaver.entity.Expense
import com.example.expensessaver.ui.details.DetailFragmentArgumentEntity
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class ExpenditureListFragment : Fragment() {
    private lateinit var binding: FragmentExpenditureBinding
    private lateinit var adapter: ExpenditureListAdapter
    private lateinit var navController: NavController
    private val viewModel: ExpenditureViewModel by viewModel()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentExpenditureBinding.inflate(inflater)
        navController = findNavController()
        adapter = ExpenditureListAdapter({
            navigateToInsert(false,it.details)
        }, { item, p ->
            deleteAlert(item, p)
        })
        binding.expenseRecycler.adapter = this.adapter


        viewModel.expensesLiveData.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            adapter.clearInsert(it)
//            when (it) {
//                is Result.Success -> {
//                    adapter.addData(it.data)
//                }
//                is Result.Error -> {
//                    showError(it.exception)
//                }
//            }
            viewModel.resetList()
        })

        viewModel.deleteLiveData.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            when (it) {
                is Result.Success -> {
                    adapter.removeItem(it.data)
                }
                is Result.Error -> {
                    showError(it.exception)
                }
            }
            viewModel.resetPosition()
        })

        binding.floatingActionButton.setOnClickListener {
            navigateToInsert(true,"")
        }
        return binding.root
    }

    private fun showError(e: Exception) {
        Timber.e(e)
        Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
    }

    override fun onResume() {
        super.onResume()
//        viewModel.getExpenses()
    }

    fun navigateToInsert(isInsert: Boolean = true,details:String) {
        val expense = Expense(0,0.0, details,"", 0,0L,0)
        val data = DetailFragmentArgumentEntity(isInsert = isInsert,expense = expense)
        navController.navigate(
            ExpenditureListFragmentDirections.actionExpenditureFragmentToDetailsFragment(
                data
            )
        )
    }

    private fun deleteAlert(item: Expense, position: Int) {
        val alert = AlertDialog.Builder(requireContext())
        alert.setTitle("Sure to delete item ?")
        alert.setPositiveButton(
            "Yes"
        ) { dialog, _ ->
            dialog.dismiss()
            viewModel.deleteItem(item, position)
        }
        alert.setNegativeButton(
            "No"
        ) { dialog, _ ->
            dialog.dismiss()
        }
        alert.show()
    }
}
package com.example.expensessaver.ui.expenditure

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.expensessaver.api.Result
import com.example.expensessaver.entity.Expense
import com.example.expensessaver.repo.ExpenseRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class ExpenditureViewModel(private val expenseRepo: ExpenseRepo) : ViewModel() {

    private val _expensesLiveData = MutableLiveData<Result<List<Expense>>?>()


    val expensesLiveData: LiveData<List<Expense>> = expenseRepo.listLiveData
    val listByCatCapLiveData: LiveData<List<Expense>> = expenseRepo.listByCatCapLiveData

    val deleteLiveData = MutableLiveData<Result<Int>>()

    fun getListByCatCapLiveData(categoryId: Int, capitalId: Int) {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                expenseRepo.getListByCatCapLiveData(categoryId, capitalId)
            }
        }
    }

    fun resetList() {
        _expensesLiveData.postValue(null)
    }

    fun resetPosition() {
        deleteLiveData.postValue(null)
    }

    fun deleteItem(expense: Expense, adapterPosition: Int) {
        viewModelScope.launch {
            val deleteRes = expenseRepo.delete(expense, adapterPosition)
            deleteLiveData.postValue(deleteRes)
            Timber.d("result $deleteRes")
        }
    }

}
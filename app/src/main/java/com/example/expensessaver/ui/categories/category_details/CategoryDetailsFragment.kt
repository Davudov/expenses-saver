package com.example.expensessaver.ui.categories.category_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.expensessaver.databinding.FragmentCategoryDetailsBinding
import com.example.expensessaver.ui.reserv.ReserveViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class CategoryDetailsFragment : Fragment() {
    lateinit var binding: FragmentCategoryDetailsBinding
    private val arg by navArgs<CategoryDetailsFragmentArgs>()
    private val navController: NavController by lazy { findNavController() }
    private val viewModel: ReserveViewModel by viewModel()
    private val adapter = CapitalListAdapter() {
        navController.navigate(
            CategoryDetailsFragmentDirections.actionNavCategoryDetailsToCategoryCapitalDetailsFragment(
                CatCaplExpArgEntity(it.category_limit_category_id,it.reserve_capital_id)
            )
        )
    }
    private val dataObserver: Observer<List<CapitalExpense>> by lazy {
        Observer {
            it ?: return@Observer
            Timber.d("expenseByCapitalDate size ${it.size}")
            adapter.insertData(it)
            viewModel.resetList()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCategoryDetailsBinding.inflate(inflater)
        viewModel.getLatestExpenseDataByCategory(arg.item.id)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.capitalList.adapter = adapter
        viewModel.expenseByCapitalDate.observe(viewLifecycleOwner, dataObserver)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.expenseByCapitalDate.removeObserver(dataObserver)
    }


}
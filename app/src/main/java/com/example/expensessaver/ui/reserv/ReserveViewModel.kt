package com.example.expensessaver.ui.reserv

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.expensessaver.api.Result
import com.example.expensessaver.entity.Category
import com.example.expensessaver.entity.ReserveCapital
import com.example.expensessaver.entity.ReserveCapitalDetails
import com.example.expensessaver.repo.ReserveRepo
import com.example.expensessaver.ui.categories.category_details.CapitalExpense
import com.example.expensessaver.ui.details.DetailsViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class ReserveViewModel(private val reserveRepo: ReserveRepo) : ViewModel() {

    val resTotalExpense:LiveData<ReserveCapitalDetails> = reserveRepo.resTotalExpense
    val categoryList = reserveRepo.categoryList
    private val _reservedCapitalList = MutableLiveData<Result<List<ReserveCapital>>?>()

    val reservedCapitalList: LiveData<List<ReserveCapital>> = reserveRepo.reservedCapitalList

    private val _expenseByCapitalDate: MutableLiveData<List<CapitalExpense>> =
        reserveRepo.expenseByCapitalDate

    val expenseByCapitalDate: LiveData<List<CapitalExpense>> =
        _expenseByCapitalDate

    val deleteLiveData = MutableLiveData<Result<Int>>()
    val insertLiveData = MutableLiveData<DetailsViewModel.ViewModelData>()

    fun deleteItem(capital: ReserveCapital, adapterPosition: Int) {
        viewModelScope.launch {
            val deleteRes = reserveRepo.delete(capital, adapterPosition)
            deleteLiveData.postValue(deleteRes)
            Timber.d("result $deleteRes")
        }
    }

    fun deleteCategory(category: Category, adapterPosition: Int) {
        viewModelScope.launch {
            val deleteRes = reserveRepo.deleteCategory(category, adapterPosition)
            deleteLiveData.postValue(deleteRes)
            Timber.d("result $deleteRes")
        }
    }

    fun insertCategory(category: String) {
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                val insertResult = reserveRepo.insertCategory(category.trim())
                insertLiveData.postValue(
                    DetailsViewModel.ViewModelData(
                        insertResult,
                        DetailsViewModel.ACTION.ON_SUCCESS
                    )
                )
            }
        }
    }

    fun getLatestExpenseDataByCategory(category: Int) {
        viewModelScope.launch {
            reserveRepo.getDataByCategory(category)
        }
    }

    fun resetList() {
        _reservedCapitalList.postValue(null)
        _expenseByCapitalDate.postValue(null)

    }


    fun resetPosition() {
        deleteLiveData.postValue(null)
    }

    fun getTotalExpenseByCapital(capitalId: Int) {
        viewModelScope.launch {
            reserveRepo.getTotalExpenseByCapital(capitalId)
        }
    }

}
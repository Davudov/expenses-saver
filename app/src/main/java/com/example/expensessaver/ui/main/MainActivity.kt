package com.example.expensessaver.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.*
import com.example.expensessaver.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var mainNavController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        mainNavController = findNavController(R.id.nav_host_fragment)


        appBarConfiguration = AppBarConfiguration(navGraph = mainNavController.graph,drawer_layout)

        setupActionBarWithNavController(mainNavController, drawer_layout)

        toolbar.setupWithNavController(mainNavController,appBarConfiguration)

        nav_view.setNavigationItemSelectedListener {dest ->
            when(dest.itemId) {
                else -> {
                    NavigationUI.onNavDestinationSelected(dest, mainNavController)
                    drawer_layout.closeDrawers()
                }
            }

            true
        }


    }


    override fun onSupportNavigateUp(): Boolean {
        return mainNavController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

//
//    override fun onSupportNavigateUp(): Boolean {
//        return NavigationUI.navigateUp(mainNavController, drawer_layout)
//    }


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


}

package com.example.expensessaver.ui.categories

import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.expensessaver.R
import com.example.expensessaver.ui.details.DetailsViewModel
import com.example.expensessaver.ui.reserv.ReserveViewModel
import com.example.expensessaver.util.ServiceUtil
import kotlinx.android.synthetic.main.fragment_new_category.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewCategoryFragment():Fragment(R.layout.fragment_new_category) {
    private val viewModel: ReserveViewModel by viewModel()
    private lateinit var navController: NavController

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navController = findNavController()
        save_category_btn.setOnClickListener{
            val category = edit_text_category_name.text.toString()
            if (category.isEmpty()){
                showAlert()
            }else{
                viewModel.insertCategory(category)
            }
        }
        edit_text_category_name.requestFocus()

        ServiceUtil.showKeyboard(edit_text_category_name,requireContext())

        viewModel.insertLiveData.observe(viewLifecycleOwner, Observer {
            when(it.action){
                DetailsViewModel.ACTION.ON_SUCCESS->{
                    navController.navigateUp()
                }
            }
        })
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }


    private fun hideKeyboard() {
        val imm: InputMethodManager = ServiceUtil.getInputMethodManager(
            requireContext()
        )
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    private fun showAlert(){
        Toast.makeText(
            requireContext(),
            "Edit category details",
            Toast.LENGTH_LONG
        ).show()
        edit_text_category_name.error = "Edit category"
    }
}
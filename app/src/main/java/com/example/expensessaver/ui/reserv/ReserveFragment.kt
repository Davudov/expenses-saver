package com.example.expensessaver.ui.reserv

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.expensessaver.api.Result
import com.example.expensessaver.databinding.ReserveLayoutBinding
import com.example.expensessaver.entity.ReserveCapital
import com.example.expensessaver.ui.categories.category_details.CapitalDetailsArgEntity
import com.example.expensessaver.ui.details.ReserveDetailFragmentArgumentEntity
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class ReserveFragment : Fragment() {
    private val viewModel: ReserveViewModel by viewModel()
    private lateinit var binding: ReserveLayoutBinding
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ReserveLayoutBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        navController = findNavController()
        val adapter = ReserveListAdapter({
            navController.navigate(
                ReserveFragmentDirections.actionNavReserveToCapitalDetailsFragment(
                    CapitalDetailsArgEntity(it.reserve_capital_id)
                )
            )
        }, { c, p ->
            deleteAlert(c, p)
        })


        viewModel.deleteLiveData.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            when (it) {
                is Result.Success -> {
                    adapter.removeItem(it.data)
                }
                is Result.Error -> {
                    showError(it.exception)
                }
            }
            viewModel.resetPosition()
        })


        binding.reserveList.adapter = adapter


        viewModel.reservedCapitalList.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            adapter.clearInsert(it)
            viewModel.resetList()
        })

        binding.insertBtn.setOnClickListener {
            navigateToInsert()
        }


    }


    fun navigateToInsert(isInsert: Boolean = true) {
        navController.navigate(
            ReserveFragmentDirections.actionNavReserveToReserveDetailsFragment(
                ReserveDetailFragmentArgumentEntity(isInsert = isInsert)
            )
        )
    }


    private fun deleteAlert(capital: ReserveCapital, position: Int) {
        val alert = AlertDialog.Builder(requireContext())
        alert.setTitle("Sure to delete item ?")
        alert.setPositiveButton(
            "Yes"
        ) { dialog, _ ->
            dialog.dismiss()
            viewModel.deleteItem(capital, position)
        }
        alert.setNegativeButton(
            "No"
        ) { dialog, _ ->
            dialog.dismiss()
        }
        alert.show()
    }


    private fun showError(e: Exception) {
        Timber.e(e)
        Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
    }

}
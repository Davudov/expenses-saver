package com.example.expensessaver.ui.reserv.edit_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.expensessaver.api.Result
import com.example.expensessaver.databinding.FragmentReserveDetailsBinding
import com.example.expensessaver.entity.CategoryLimits
import com.example.expensessaver.ui.details.DetailsViewModel
import com.example.expensessaver.util.ServiceUtil
import kotlinx.android.synthetic.main.fragment_new_category.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ReserveDetailsFragment : Fragment() {

    private val arg by navArgs<ReserveDetailsFragmentArgs>()

    private lateinit var binding: FragmentReserveDetailsBinding

    private lateinit var navController: NavController

    private val viewModel: DetailsViewModel by viewModel()
    private lateinit var arrayAdapter: ArrayAdapter<String>
    private lateinit var categoryLimitAdapter: CategoryLimitAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentReserveDetailsBinding.inflate(inflater)

        navController = findNavController()

        binding.item = arg.item

        binding.viewModel = viewModel

        binding.editTextAmount.requestFocus()
        ServiceUtil.showKeyboard(binding.editTextAmount,requireContext())

        arrayAdapter = ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item
        )

//        binding.categoryLimitRecycler.addItemDecoration(DividerItemDecoration(requireContext(),LinearLayoutManager.VERTICAL))
        categoryLimitAdapter = CategoryLimitAdapter()

        binding.save.setOnClickListener {
            val item = arg.item
            item.capital.amountStr = binding.editTextAmount.text.toString()
            item.capital.details = binding.editTextDetailsReserveCap.text.toString()
            viewModel.saveReserve(item.capital,categoryLimitAdapter.currentList)
        }

        binding.categoryLimitRecycler.adapter = categoryLimitAdapter

        viewModel.insertionLiveData.observe(viewLifecycleOwner, Observer {
            when (it.action) {
                DetailsViewModel.ACTION.ON_SUCCESS -> when (it.result) {
                    is Result.Success -> {
                        navController.navigateUp()
                    }
                    is Result.Error -> {
                        Toast.makeText(
                            requireContext(),
                            it.result.exception.message,
                            Toast.LENGTH_LONG
                        )
                            .show()
                    }
                }
                DetailsViewModel.ACTION.ON_AMOUNT_EMPTY -> {
                    Toast.makeText(
                        requireContext(),
                        "Add amount to expenditure details",
                        Toast.LENGTH_LONG
                    ).show()
                    binding.editTextAmount.error = "Add amount"
                }
            }

        })


        viewModel.categoryList.observe(viewLifecycleOwner, Observer { result ->
            result ?: return@Observer
            categoryLimitAdapter.insertData(result.map {
                CategoryLimits(
                    0,
                    it.category_id,
                    0,
                    .0,
                    it.categoryName,
                    .0
                )
            })
        })
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }

    private fun hideKeyboard() {
        val imm: InputMethodManager = ServiceUtil.getInputMethodManager(
            requireContext()
        )
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)
    }


}

package com.example.expensessaver.ui.categories.category_details

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CatCaplExpArgEntity (val categoryId:Int, val capitalId:Int):
    Parcelable
@Parcelize
data class CapitalDetailsArgEntity (val capitalId:Int):
    Parcelable
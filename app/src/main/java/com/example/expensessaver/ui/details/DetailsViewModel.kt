package com.example.expensessaver.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.expensessaver.api.Result
import com.example.expensessaver.entity.CategoryLimits
import com.example.expensessaver.entity.Expense
import com.example.expensessaver.entity.ReserveCapital
import com.example.expensessaver.entity.convertStrToDoubleSafely
import com.example.expensessaver.repo.ExpenseRepo
import com.example.expensessaver.repo.ReserveRepo
import kotlinx.coroutines.launch

class DetailsViewModel(private val expenseRepo: ExpenseRepo, private val reserveRepo: ReserveRepo) :
    ViewModel() {

    private val _insertionLiveData = MutableLiveData<ViewModelData>()
    val insertionLiveData: LiveData<ViewModelData> = _insertionLiveData


    val categoryList = reserveRepo.categoryList
    val availableCategoryList = reserveRepo.availableCategoryList

    fun saveExpense(expense: Expense?) {
        expense ?: return
        expense.expenseDateMillis = System.currentTimeMillis()
        if (expense.amountStr.isEmpty())
            _insertionLiveData.postValue(ViewModelData(null, ACTION.ON_AMOUNT_EMPTY))
        else {
            viewModelScope.launch {
                expense.amountDouble = expense.amountStr.convertStrToDoubleSafely()
                val expenseResult = expenseRepo.insertItem(expense)
                _insertionLiveData.postValue(ViewModelData(expenseResult, ACTION.ON_SUCCESS))
            }
        }
    }

    fun saveReserve(reserveCapital: ReserveCapital?,currentList: MutableList<CategoryLimits>) {
        reserveCapital ?: return
        reserveCapital.reserveCapitalDateMillis = System.currentTimeMillis()
        if (reserveCapital.amountStr.isEmpty())
            _insertionLiveData.postValue(ViewModelData(null, ACTION.ON_AMOUNT_EMPTY))
        else {
            viewModelScope.launch {
                reserveCapital.amount = reserveCapital.amountStr.convertStrToDoubleSafely()
                val expenseResult = reserveRepo.insertCapital(reserveCapital,currentList)
                _insertionLiveData.postValue(ViewModelData(expenseResult, ACTION.ON_SUCCESS))
            }
        }
    }

    enum class ACTION {
        ON_AMOUNT_EMPTY,
        ON_SUCCESS
    }

    class ViewModelData(val result: Result<Boolean>?, val action: ACTION)
}

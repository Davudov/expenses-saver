package com.example.expensessaver.ui.expenditure

import com.example.expensessaver.R
import com.example.expensessaver.base.BaseRecyclerViewAdapter
import com.example.expensessaver.entity.Expense

class ExpenditureListAdapter(onClick: (expense: Expense) -> Unit,onLongClick: (expense: Expense,pos:Int) -> Unit) :
    BaseRecyclerViewAdapter<Expense>(onClick,onLongClick) {
    override fun getLayoutRes(viewType: Int) = R.layout.list_item_expense
}

package com.example.expensessaver.ui.categories.category_details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.expensessaver.databinding.ItemViewExpenseTotalBriefBinding
import com.example.expensessaver.entity.CapitalDetails


//const val CAPITAL_VIEW_TYPE = 0
//const val EXPENSE_VIEW_TYPE = 1
const val TOTAL_EXP_VIEW_TYPE = 2

class CapitalListAdapter (val onClick:(expense:CapitalDetails)->Unit):
    ListAdapter<CapitalExpense, RecyclerView.ViewHolder>(CapitalDiffCallback()) {

    fun insertData(list: List<CapitalExpense>) {
        submitList(list)
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
//            is CapitalExpense.Capital -> CAPITAL_VIEW_TYPE
//            is CapitalExpense.Expense -> EXPENSE_VIEW_TYPE
            is CapitalExpense.Total -> TOTAL_EXP_VIEW_TYPE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TOTAL_EXP_VIEW_TYPE -> {
                TotalExpenseViewHolder.from(parent,onClick)
            }
            else -> throw ClassCastException("Unknown viewType ${viewType}")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TotalExpenseViewHolder -> {
                holder.bind(getItem(position) as CapitalExpense.Total)
            }
        }
    }

    class TotalExpenseViewHolder(val binding: ItemViewExpenseTotalBriefBinding,val onClick: (expense: CapitalDetails) -> kotlin.Unit, ) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup, onClick: (expense: CapitalDetails) -> kotlin.Unit): TotalExpenseViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding =
                    ItemViewExpenseTotalBriefBinding.inflate(layoutInflater, parent, false)
                return TotalExpenseViewHolder(binding,onClick)
            }
        }

        fun bind(total: CapitalExpense.Total) {
            binding.item = total.capitalDetails
            binding.executePendingBindings()
            binding.root.setOnClickListener { onClick(total.capitalDetails) }
        }

    }

}


class CapitalDiffCallback : DiffUtil.ItemCallback<CapitalExpense>() {
    override fun areItemsTheSame(oldItem: CapitalExpense, newItem: CapitalExpense): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: CapitalExpense, newItem: CapitalExpense): Boolean {
        return oldItem == newItem
    }
}

sealed class CapitalExpense {
    class Total(val capitalDetails: CapitalDetails) : CapitalExpense()
}
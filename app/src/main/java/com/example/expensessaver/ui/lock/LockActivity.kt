package com.example.expensessaver.ui.lock

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Vibrator
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.expensessaver.BuildConfig
import com.example.expensessaver.R
import com.example.expensessaver.extensions.hideKeyboard
import com.example.expensessaver.ui.main.MainActivity
import com.example.expensessaver.util.ServiceUtil
import kotlinx.android.synthetic.main.activity_lock.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class LockActivity : Activity() {
    val TIMING_ANIM = 500L
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lock)
        val shakeAnim = AnimationUtils.loadAnimation(this,R.anim.shake)
        val vibrator: Vibrator = ServiceUtil.getVibrator(this@LockActivity)
        unlockApp()
        lock_icon.apply {
            setOnClickListener {
                hideKeyboard()
                if (password_et.text.toString() == BuildConfig.APP_LOCK) {
                    setImageDrawable(
                        ContextCompat.getDrawable(
                            this@LockActivity,
                            R.drawable.ic_baseline_lock_open_24
                        )
                    )
                    GlobalScope.launch {
                        delay(1000)
                        unlockApp()
                    }
                    lock_text.text = "App is unlocked"
                } else {
                    startAnimation(shakeAnim)
                    GlobalScope.launch {
                        delay(TIMING_ANIM)
                        shakeAnim.cancel()
                    }
                    Toast.makeText(this@LockActivity, "Try again", Toast.LENGTH_LONG).show()

                    vibrator.vibrate(TIMING_ANIM)
                }
            }
        }
    }

    private fun unlockApp() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
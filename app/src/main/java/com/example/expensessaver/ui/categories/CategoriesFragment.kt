package com.example.expensessaver.ui.categories

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.expensessaver.api.Result
import com.example.expensessaver.databinding.FragmentCategoriesBinding
import com.example.expensessaver.entity.Category
import com.example.expensessaver.ui.categories.category_details.CategoryDetailsFragmentArgumentEntity
import com.example.expensessaver.ui.reserv.ReserveViewModel
import kotlinx.android.synthetic.main.fragment_categories.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class CategoriesFragment : Fragment() {

    private val viewModel: ReserveViewModel by viewModel()

    lateinit var binding: FragmentCategoriesBinding

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCategoriesBinding.inflate(inflater)
        navController = findNavController()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = CategoryListAdapter({
            navController.navigate(
                CategoriesFragmentDirections.actionNavCategoriesToNavCategoryDetails(
                    CategoryDetailsFragmentArgumentEntity(it.category_id)
                )
            )
        }, { c, p ->
            deleteAlert(c,p)
        })

        binding.floatingActionButton.setOnClickListener {
            navController.navigate(CategoriesFragmentDirections.actionNavCategoriesToNavCategoriesNew())
        }

        capital_list.adapter = adapter

        viewModel.categoryList.observe(viewLifecycleOwner, { list->
            list?:return@observe
            adapter.submitList(list)
        })



        viewModel.deleteLiveData.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            when (it) {
                is Result.Success -> {
                    adapter.notifyItemRemoved(it.data)
                }
                is Result.Error -> {
                    showError(it.exception)
                }
            }
            viewModel.resetPosition()
        })


    }

    private fun deleteAlert(item: Category, position: Int) {
        val alert = AlertDialog.Builder(requireContext())
        alert.setTitle("Sure to delete item ?")
        alert.setPositiveButton(
            "Yes"
        ) { dialog, _ ->
            dialog.dismiss()
            viewModel.deleteCategory(item, position)
        }
        alert.setNegativeButton(
            "No"
        ) { dialog, _ ->
            dialog.dismiss()
        }
        alert.show()
    }

    private fun showError(e: Exception) {
        Timber.e(e)
        Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show()
    }

}
package com.example.expensessaver

import android.app.Application
import androidx.lifecycle.LifecycleObserver
import androidx.room.Room
import com.example.expensessaver.repo.ExpenseRepo
import com.example.expensessaver.repo.ReserveRepo
import com.example.expensessaver.storage.AppDatabase
import com.example.expensessaver.storage.CapitalDao
import com.example.expensessaver.storage.ExpenseDao
import com.example.expensessaver.ui.details.DetailsViewModel
import com.example.expensessaver.ui.expenditure.ExpenditureViewModel
import com.example.expensessaver.ui.reserv.ReserveViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import timber.log.Timber

class ExpenseSaverApp : Application(), LifecycleObserver {

    companion object {
        lateinit var INSTANCE: ExpenseSaverApp
//        val DEFAULT_SHARED_PREFERENCES = "default"
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        initKoin()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initKoin() {
        val moduleDefinition = module {
            single { createDatabase() }

            single {
                createExpenseDao(get())
            }
            single {
                createCapitalDao(get())
            }

            single {
                ExpenseRepo(get())
            }
            single {
                ReserveRepo(get())
            }

            viewModel {
                DetailsViewModel(get(),get())
            }

            viewModel {
                ExpenditureViewModel(get())
            }
            viewModel {
                ReserveViewModel(get())
            }

        }
        startKoin {
            androidContext(this@ExpenseSaverApp)
            modules(listOf(moduleDefinition))
        }
    }

    private fun createDatabase(): AppDatabase {
        return Room.databaseBuilder(
            this@ExpenseSaverApp,
            AppDatabase::class.java,
            AppDatabase.name
        )
//            .addMigrations(MIGRATION_2_3)
            .fallbackToDestructiveMigration()
            .build()

    }
//
//    private val MIGRATION_1_2 = object : Migration(1, 2) {
//        override fun migrate(database: SupportSQLiteDatabase) {
//            database.execSQL("CREATE TABLE IF NOT EXISTS ReserveCapital (id INTEGER NOT NULL, capital_amount FLOAT NOT NULL, capital_details TEXT NOT NULL, created_date TEXT NOT NULL,capital_category TEXT NOT NULL, PRIMARY KEY(id) )")
//        }
//    }
//    private val MIGRATION_2_3 = object : Migration(2, 3) {
//        override fun migrate(database: SupportSQLiteDatabase) {
//            database.execSQL("CREATE TABLE IF NOT EXISTS Expense1 (id INTEGER NOT NULL, capital_amount FLOAT NOT NULL, capital_details TEXT NOT NULL, created_date TEXT NOT NULL,capital_category TEXT NOT NULL, PRIMARY KEY(id) )")
//        }
//    }

    private fun createExpenseDao(appDatabase: AppDatabase): ExpenseDao {
        return appDatabase.expenseDao()
    }

    private fun createCapitalDao(appDatabase: AppDatabase): CapitalDao {
        return appDatabase.capitalDao()
    }

}